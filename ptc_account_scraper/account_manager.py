from simplecrypt import encrypt, decrypt
import click


@click.command()
@click.option('--encrypt', 'e', flag_value=True)
@click.option('--decrypt', 'e', flag_value=False)
@click.option('--output', type=click.File('wb'))
@click.argument('file', type=click.File('rb'))
@click.argument('masterkey', envvar='MASTER_KEY', type=str)
def cli(e, output, masterkey, file):
  if e:
    ciphertext = encrypt(masterkey, file.read())
    if output:
      output.write(ciphertext)
    else:
      print ciphertext
  else:
    plaintext = decrypt(masterkey, file.read())
    if output:
      output.write(plaintext)
    else:
      print plaintext

if __name__ == '__main__':
  cli()
