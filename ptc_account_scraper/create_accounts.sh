#!/bin/bash

NUM_ACCOUNTS=$1
export $(cat ../.env | xargs)
for (( i = 0; i < $NUM_ACCOUNTS; i++ )); do
  scrapy crawl create_ptc_account --nolog -t csv -o - -a gmail_username=$GMAIL_USERNAME -a gmail_password=$GMAIL_PASSWORD | tail -1
done
