import re
import os
import requests
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), 'gmail'))
import gmail


def verify_all(username, password):
  g = gmail.login(username, password)
  emails = g.inbox().mail(unread=True, sender="noreply@pokemon.com")
  for email in emails:
    verify(email)


def verify(email):
  email.fetch()
  verify_link = re.search('https://[^\s]*', email.body).group()
  print verify_link
  response = requests.get(verify_link)
  print response
  if response.status_code == 200:
    email.read()
    email.archive()
