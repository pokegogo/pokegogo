# -*- coding: utf-8 -*-
import scrapy
import radar
import random
import string
from ptc_account_scraper.items import Account
from ptc_account_scraper import verify_emails


class CreatePtcAccountSpider(scrapy.Spider):
    name = "create_ptc_account"
    allowed_domains = ["club.pokemon.com", "sso.pokemon.com"]
    start_urls = (
        'https://club.pokemon.com/us/pokemon-trainer-club/sign-up/',
    )

    def __init__(self, gmail_username, gmail_password, *args, **kwargs):
      super(CreatePtcAccountSpider, self).__init__(*args, **kwargs)
      self.gmail_username = gmail_username
      self.gmail_password = gmail_password

    def on_error(self, error):
      raise Exception(error)
      #  print "Got an error! {}".format(error)

    def parse(self, response):
      dob = radar.random_datetime(start='1980-01-01', stop='1997-01-01').strftime('%Y-%m-%d')
      return scrapy.FormRequest.from_response(
        response,
        formname='verify-age',
        formdata={
          'dob': dob,
          'country': 'US',
        },
        callback=self.second_page,
        errback=self.on_error)

    def second_page(self, response):
      #  print "got to second page: {}".format(response.body)
      username = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(15))
      password = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(15))
      email = 'pokegogotest+{}@gmail.com'.format(username)
      #  print('username = {}, password = {}'.format(username, password))
      account = Account(email=email, username=username, password=password)
      return scrapy.FormRequest.from_response(
        response,
        formname='create-account',
        formdata={
          'username': username,
          'password': password,
          'confirm_password': password,
          'email': email,
          'confirm_email': email,
          'public_profile_opt_in': 'False',
          'terms': 'on'
        },
        callback=self.final_page,
        errback=self.on_error,
        meta={'account': account})

    def final_page(self, response):
      #  print response.body
      #  return account
      # Accept term and condition
      return scrapy.Request(url='https://sso.pokemon.com/sso/login?locale=en&service=https://club.pokemon.com/us/pokemon-trainer-club/caslogin',
                            callback=self.login,
                            errback=self.on_error,
                            meta=response.meta)

    def login(self, response):
      account = response.meta['account']
      return scrapy.FormRequest.from_response(
        response,
        formcss='#login-form',
        formdata={
          'username': account['username'],
          'password': account['password'],
        },
        callback=self.profile_page,
        errback=self.on_error,
        meta=response.meta)

    def profile_page(self, response):
      return scrapy.Request(url='https://club.pokemon.com/us/pokemon-trainer-club/go-settings',
                            callback=self.accept_tos,
                            errback=self.on_error,
                            meta=response.meta)

    def accept_tos(self, response):
      return scrapy.FormRequest.from_response(
        response,
        formnumber=1,
        formdata={
          'go_terms': 'on'
        },
        callback=self.done,
        errback=self.on_error,
        meta=response.meta)

    def done(self, response):
      if response.status == 200:
        verify_emails.verify_all(self.gmail_username, self.gmail_password)
        return response.meta['account']
