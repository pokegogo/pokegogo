# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Account(scrapy.Item):
    # define the fields for your item here like:
    email = scrapy.Field()
    username = scrapy.Field()
    password = scrapy.Field()
