import os
import time
import pytz
from datetime import datetime
from s2sphere import LatLng
from pushbullet import Pushbullet


api_key = os.environ['PUSHBULLET_ACCESS_TOKEN']
pb = Pushbullet(api_key)


def notify(poke, current_lat, current_lng):

  # http://maps.google.com/maps/place/<place_lat>,<place_long>/@<map_center_lat>,<map_center_long>,<zoom_level>z
  lat_lng = '{},{}'.format(poke["lat"], poke["lng"])
  url = 'https://maps.google.com?saddr=Current+Location&daddr={}'.format(lat_lng)
  print('url = {}'.format(url))

  # Compute distance and direction
  text = "A wild {} is nearby!".format(poke['name'])
  # TODO Assume PST/PDT
  gone_at = datetime.utcfromtimestamp(poke['timestamp'] + poke['timeleft']) \
                    .replace(tzinfo=pytz.utc) \
                    .astimezone(pytz.timezone('US/Pacific')) \
                    .strftime("%-I:%M%p")

  origin = LatLng.from_degrees(current_lat, current_lng)
  poke_latlng = LatLng.from_degrees(poke['lat'], poke['lng'])
  delta = poke_latlng - origin
  lat = delta.lat().degrees
  lng = delta.lng().degrees
  direction = (('N' if lat >= 0 else 'S') if abs(lat) > 1e-4 else '') + (('E' if lng >= 0 else 'W') if abs(lng) > 1e-4 else '')
  body = "It's {}m {} of you. Disppears at {}. Hurry!".format(int(origin.get_distance(poke_latlng).radians *
                                                                  6366468.241830914), direction, gone_at)
  print("body = {}".format(body))
  response = pb.push_link(text, url, body=body)
  print('Pushbullet response: {}'.format(response))


if __name__ == '__main__':
  pokemon = {
    'name': 'Pidgey',
    'lat': 47.612664,
    'lng': -122.204259,
    'timestamp': time.time(),
    'timeleft': 900
  }
  notify(pokemon, 47.57974596900608, -122.1272053365846)
