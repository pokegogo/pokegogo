import logging
import requests
import os
import re
import struct
import json
import pokemon_pb2
import time
import random
from simplecrypt import decrypt

from google.protobuf.internal import encoder
from datetime import datetime, timedelta
import geopy.distance
from s2sphere import CellId, LatLng
from celery import Celery, group
import redis
import urlparse

from celery.utils.log import get_task_logger
from celery.signals import celeryd_init, celeryd_after_setup
from celery.task.control import discard_all

from .notification import notify

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

logger = get_task_logger(__name__)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger('celery').setLevel(logging.WARNING)


# This is the Celery task used for scanning nearby pokemons

# Data model:
# locations - This is a geo cache in Redis. Each value is an account, and the lat/lng is the location that this account
# scanned last. This cache is used to do a nearest neighbor search to figure out the best account to use to perform a
# new scan. We want accounts that are as close to the new scan location as possible to avoid being flagged as
# teleporting
# ptc_session_{username} - This is a Redis hash that stores the following for each PTC account we have:
# last_login: Last time this account loggin
# access_token: Used to authenticate requests to Niantic
# username: Username for the PTC account
# api_endpoint: Which endpoint we use to talk to Niantic
# useauth: The serialized ticket used to authenticate requests to Niantic
# The session data is updated initially during login, and refreshed every 25 minutes via a Celery beat job, so our
# accounts don't timeout when the ticket expires
# cell:{cell_id} - This is a set that contains all the pokemon keys. The UI uses this value to load all the pokemons.
# The set gets updated whenever a new pokemon is detected from scanning, but since pokemons expire on their own, we
# don't have an automatic way to update this set. It's currently updated lazily by the UI. When the UI loads all the
# pokemons from this cell, it checks to make sure that the Pokemon still exists in Redis, and update this set
# accordingly.
# pokemon_hash = spawnpoint_id:pokemon_id
# pokemon:{pokemon_hash} - This is a simple key in Redis that expires when according to the expiration ttl from the
# server response
# cell:{cell_id}_last_scanned - This is a timestamp of the last time this cell was scanned. This is to prevent cells
# from being scanned too frequently. We cache the result of a cell for a random period of time to prevent flodding the
# backend with too many simultaneous requests
# cell:{cell_id}_last_scanned_by - This is the username that scanned this cell last. Currently used purely for debugging
# purpose
# cell:{cell_id}_locked - This is a lock indicating that this cell is currently being scanned by an account. This is to
# prevent multiple accounts from scanning the same cell


REDIS_URL = os.getenv('REDIS_URL', 'redis://redis:6379')
app = Celery('scanner', broker=REDIS_URL)
url = urlparse.urlparse(REDIS_URL)
db = redis.StrictRedis(host=url.hostname, port=url.port, password=url.password)


# Refresh login every 25 minutes, currenlty login token expires after 30 minutes
app.conf.CELERYBEAT_SCHEDULE = {
  'refresh_session': {
    'task': 'refresh_session',
    'schedule': timedelta(minutes=25),
    'kwargs': {'purge' : False},
    'relative': True
  }
}

app.conf.CELERY_ROUTES = {
  'refresh_session': {'queue': 'refresher'},
  'refresh_single_account': {'queue': 'refresher'},
  'scan_cell': {'queue': 'scanner'}
}
app.conf.CELERY_REDIRECT_STDOUTS = True
app.conf.CELERY_REDIRECT_STDOUTS_LEVEL = 'INFO'


@celeryd_after_setup.connect
#  @celeryd_init.connect
def init_worker(sender=None, conf=None, **kwargs):
  """
  Perform some initialization when the worker pool starts for the first time:
  1. Deleting all pending tasks, so we start fresh every time
  2. Load all the accounts information from file and decrypt them
  3. Login all the accounts (TODO: We should do this in a separate queue, otherwise everytime account is refreshed, it
  blocks up all scanning workers)
  """
  if sender != 'celery@refresher':
    return
  logger.info('Initializing worker {}'.format(sender))
  logger.info('Deleting all pending tasks')
  app.control.purge()
  #  discard_all()

  MASTERKEY = os.getenv('MASTER_KEY')
  if not MASTERKEY:
    raise Exception("Master key is not found!")

  print('Loading accounts...')
  with open(os.path.join(os.path.dirname(__file__), 'accounts.csv.enc'), 'rb') as f:
    plaintext = decrypt(MASTERKEY, f.read())
    global ACCOUNTS
    ACCOUNTS = [line.split(',')[:2] for line in plaintext.split('\n')]

  # Refresh all sessions on start, and purge existing data
  refresh_session.delay(purge=True)


# TODO Why are these globals??
COORDS_LATITUDE = 0
COORDS_LONGITUDE = 0
COORDS_ALTITUDE = 0
FLOAT_LAT = 0
FLOAT_LONG = 0

SLEEP_IN_SEC = 0.2  # How long to sleep before making consecutive Niantic calls
SLEEP_IN_SEC_LONG = 1  # How long to sleep before making consecutive Niantic calls
MAX_ATTEMPTS_FOR_HEARTBEAT = 3
MAX_ATTEMPTS_API_ERROR = 3
SERVER_TIMEOUT = 3
CACHE_BOUNDS = (60, 120)  # After a cell has been scanned, this amount of random wait prevents it from being scanned again
NUM_ACCOUNTS_TO_USE = 1000  # How many accounts to use from our account pool. Note that the accounts are splits in two for the two sides of the world


API_URL = 'https://pgorelease.nianticlabs.com/plfe/rpc'
LOGIN_URL = 'https://sso.pokemon.com/sso/login?service=https%3A%2F%2Fsso.pokemon.com%2Fsso%2Foauth2.0%2FcallbackAuthorize'
LOGIN_OAUTH = 'https://sso.pokemon.com/sso/oauth2.0/accessToken'
POKEMONS = json.load(open(os.path.join(os.path.dirname(__file__), 'pokemon.json')))
NOTIFY_LIST = json.load(open(os.path.join(os.path.dirname(__file__), 'notify.json')))
NOTIFY_LIST_IDS = set(map(lambda p: int(p['Number']), NOTIFY_LIST))


class PTCServerException(Exception):
  # When Pokemon Trainer Club is down
  pass


class POGOServerException(Exception):
  # When Pokemon Go server is down
  pass


def encode(cellid):
    output = []
    encoder._VarintEncoder()(output.append, cellid)
    return ''.join(output)


def get_s2_cells_around(latitude, longitude, level, spread):
  distance = geopy.distance.VincentyDistance(meters=spread)
  center = (latitude, longitude, 0)
  p1 = distance.destination(point=center, bearing=45)
  p2 = distance.destination(point=center, bearing=225)
  p1 = LatLng.from_degrees(p1[0], p1[1])
  p2 = LatLng.from_degrees(p2[0], p2[1])
  rect = LatLngRect.from_point_pair(p1, p2)
  region = RegionCoverer()
  region.min_level = level
  region.max_level = level
  cells = region.get_covering(rect)
  return sorted([c.id() for c in cells])


def f2i(float):
  return struct.unpack('<Q', struct.pack('<d', float))[0]


def f2h(float):
  return hex(struct.unpack('<Q', struct.pack('<d', float))[0])


def h2f(hex):
  return struct.unpack('<d', struct.pack('<Q', int(hex, 16)))[0]


def set_location_coords(lat, lng, alt):
  global COORDS_LATITUDE, COORDS_LONGITUDE, COORDS_ALTITUDE
  global FLOAT_LAT, FLOAT_LONG
  FLOAT_LAT = lat
  FLOAT_LONG = lng
  COORDS_LATITUDE = f2i(lat)  # 0x4042bd7c00000000 # f2i(lat)
  COORDS_LONGITUDE = f2i(lng)  # 0xc05e8aae40000000 #f2i(long)
  COORDS_ALTITUDE = f2i(alt)


def get_location_coords():
    return (COORDS_LATITUDE, COORDS_LONGITUDE, COORDS_ALTITUDE)


def api_req(httpsession, api_endpoint, access_token, *mehs, **kw):
    attempts = 0
    while True:
        try:
            p_req = pokemon_pb2.RequestEnvelop()
            p_req.rpc_id = 1469378659230941192

            p_req.unknown1 = 2

            p_req.latitude, p_req.longitude, p_req.altitude = get_location_coords()

            p_req.unknown12 = 989

            if 'useauth' not in kw or not kw['useauth']:
                p_req.auth.provider = 'ptc'
                p_req.auth.token.contents = access_token
                p_req.auth.token.unknown13 = 14
            else:
                p_req.unknown11.unknown71 = kw['useauth'].unknown71
                p_req.unknown11.unknown72 = kw['useauth'].unknown72
                p_req.unknown11.unknown73 = kw['useauth'].unknown73

            for meh in mehs:
                p_req.MergeFrom(meh)

            protobuf = p_req.SerializeToString()

            r = httpsession.post(api_endpoint, data=protobuf, verify=False, timeout=SERVER_TIMEOUT)

            if r.status_code != 200:
              raise Exception('Non 200 status code. Code:{}, Body:{}'.format(r.status_code, r.content))

            p_ret = pokemon_pb2.ResponseEnvelop()
            p_ret.ParseFromString(r.content)

            #  time.sleep(SLEEP_IN_SEC)
            return p_ret
        except Exception:
          logger.exception('API request error, retrying')
          attempts += 1
          if attempts > MAX_ATTEMPTS_API_ERROR:
            logger.info('Max attempt exceeded for API request')
            raise Exception('Max attempt exceeded for API request')
          time.sleep(SLEEP_IN_SEC_LONG)


def get_profile(httpsession, access_token, api, useauth, *reqq):
    req = pokemon_pb2.RequestEnvelop()

    req1 = req.requests.add()
    req1.type = 2
    if len(reqq) >= 1:
        req1.MergeFrom(reqq[0])

    req2 = req.requests.add()
    req2.type = 126
    if len(reqq) >= 2:
        req2.MergeFrom(reqq[1])

    req3 = req.requests.add()
    req3.type = 4
    if len(reqq) >= 3:
        req3.MergeFrom(reqq[2])

    req4 = req.requests.add()
    req4.type = 129
    if len(reqq) >= 4:
        req4.MergeFrom(reqq[3])

    req5 = req.requests.add()
    req5.type = 5
    if len(reqq) >= 5:
        req5.MergeFrom(reqq[4])
    return api_req(httpsession, api, access_token, req, useauth=useauth)


def get_api_endpoint(httpsession, access_token, api=API_URL):
  p_ret = get_profile(httpsession, access_token, api, None)
  if not p_ret.api_url:
    raise Exception('API endpoint is empty')
  return 'https://%s/rpc' % p_ret.api_url


def login_ptc(httpsession, username, password):
    try:
      head = {'User-Agent': 'niantic'}
      r = httpsession.get(LOGIN_URL, headers=head)
      logger.debug('Login response: {}'.format(r.content))
      jdata = json.loads(r.content)
      data = {
          'lt': jdata['lt'],
          'execution': jdata['execution'],
          '_eventId': 'submit',
          'username': username,
          'password': password,
      }
      r1 = httpsession.post(LOGIN_URL, data=data, headers=head)

      ticket = re.sub('.*ticket=', '', r1.history[0].headers['Location'])

      data1 = {
          'client_id': 'mobile-app_pokemon-go',
          'redirect_uri': 'https://www.nianticlabs.com/pokemongo/error',
          'client_secret': 'w8ScCUXJQc6kXKw8FiOhd8Fixzht18Dq3PEVkUCP5ZPxtgyWsbTvWHFLm2wNY0JR',
          'grant_type': 'refresh_token',
          'code': ticket,
      }
      r2 = httpsession.post(LOGIN_OAUTH, data=data1)
      logger.debug('Response[{}]: {}'.format(r2.status_code, r2.content))
      access_token = re.sub('&expires.*', '', r2.content)
      access_token = re.sub('.*access_token=', '', access_token)
      if not access_token:
        raise Exception('Access token not found')
      return access_token
    except Exception as e:
      logger.exception('Cannot login to Pokemon Trainer Club')
      raise PTCServerException(e)


def raw_heartbeat(session, httpsession):
    m4 = pokemon_pb2.RequestEnvelop.Requests()
    m = pokemon_pb2.RequestEnvelop.MessageSingleInt()
    m.f1 = int(time.time() * 1000)
    m4.message = m.SerializeToString()
    m5 = pokemon_pb2.RequestEnvelop.Requests()
    m = pokemon_pb2.RequestEnvelop.MessageSingleString()
    m.bytes = "05daf51635c82611d1aac95c0b051d3ec088a930"
    m5.message = m.SerializeToString()

    # Append all neighboring level 15 cells. For a total of 10 cells
    parent = CellId.from_lat_lng(LatLng.from_degrees(FLOAT_LAT, FLOAT_LONG)).parent(15)
    walk = [cell.id() for cell in parent.get_all_neighbors(15)]
    walk.append(parent.id())

    m1 = pokemon_pb2.RequestEnvelop.Requests()
    m1.type = 106
    m = pokemon_pb2.RequestEnvelop.MessageQuad()
    m.f1 = ''.join(map(encode, walk))
    m.f2 = "\000" * len(walk)
    m.lat = COORDS_LATITUDE
    m.long = COORDS_LONGITUDE
    m1.message = m.SerializeToString()
    useauth = pokemon_pb2.UnknownAuth()
    useauth.ParseFromString(session['useauth'])
    response = get_profile(
        httpsession,
        session['access_token'],
        session['api_endpoint'],
        useauth,
        m1,
        pokemon_pb2.RequestEnvelop.Requests(),
        m4,
        pokemon_pb2.RequestEnvelop.Requests(),
        m5)
    if response.unknown1 == 102:  # This seems to suggest that the request was throttled
      raise POGOServerException('Request throttled for account {}'.format(session['username']))
    payload = response.payload[0]
    heartbeat = pokemon_pb2.ResponseEnvelop.HeartbeatPayload()
    heartbeat.ParseFromString(payload)
    return heartbeat


def cur_time_sec():
  return int(time.time())


def update_server_status(server, status):
  db.hmset('{}_SERVER_STATUS'.format(server), {
    'status': status,
    'updated': cur_time_sec()
  })


def get_server_status():
  pogo = db.hgetall('POGO_SERVER_STATUS')
  pogo['name'] = 'Pokemon Go Server'
  ptc = db.hgetall('PTC_SERVER_STATUS')
  ptc['name'] = 'Pokemon Trainer Club'
  return [pogo, ptc]


def heartbeat(session, httpsession):
  #  attempts = 0
  #  while True:
    #  try:
    return raw_heartbeat(session, httpsession)
    #  update_server_status('POGO', 'UP')
    #  except Exception:
      #  logger.exception('Heartbeat failed, retrying')
      #  #  update_server_status('POGO', 'DOWN')
      #  attempts += 1
      #  if attempts > MAX_ATTEMPTS_FOR_HEARTBEAT:
        #  raise Exception("Exceeded max attempts for heartbeat")


@app.task(name='refresh_session')
def refresh_session(purge=False):
  logger.info('Refreshing login sessions, purge = {}...'.format(purge))
  # Clean up locations that aren't part of our accounts
  # FIXME Do it right, we also really only want to do this in dev
  if purge:
    # Delete all last scanned data
    db.delete('locations')
    # Delete all session
    keys = db.keys('ptc_session*')
    for key in keys:
      db.delete(key)

  accounts = ACCOUNTS[:NUM_ACCOUNTS_TO_USE]
  logger.info('Logging in {} accounts'.format(len(accounts)))
  job = group(refresh_single_account.s(username, password, i) for i, (username, password) in
              enumerate(accounts))
  job.apply_async()


@app.task(bind=True, max_retries=10, name='refresh_single_account')
def refresh_single_account(self, username, password, index):
  try:
    connection = Connection(username, password)
    connection.login()
    if db.zrank('locations', username) is None:
      # Divide the world into two halves, initialize the locations to be somewhere random
      # The reason for this is somehow the georadius call in Redis can only do lookup on locations on one side of the
      # world
      db.geoadd('locations', 90 * ((index % 2) * 2 - 1), 0, username)
  except Exception as exc:
    logger.exception('Error logging in with username {}'.format(username))
    self.retry(exc=exc, countdown=5)


def get_session(username):
  return db.hgetall('ptc_session_{}'.format(username))


def update_session(session):
  db.hmset('ptc_session_{}'.format(session['username']), session)


def set_last_scanned_time(username, time):
  db.hset('ptc_session_{}'.format(username), 'last_scanned', time)


def _process_cell_response(cell, timestamp, origin_lat, origin_lng):
  logger.debug('Cell response: {}'.format(cell))
  pokemons = []
  for wild in cell.WildPokemon:
    logger.debug('DEBUG response: {}'.format(wild))
    if (wild.TimeTillHiddenMs > 1000):
      hash = wild.SpawnPointId + ':' + str(wild.pokemon.PokemonId)
      key = 'pokemon:{}'.format(hash)
      # Only update if we don't know about this already
      if not db.exists(key):
        name = POKEMONS[wild.pokemon.PokemonId - 1]['Name']
        logger.info('{}: {} in cell {}'.format(name, hash, cell.S2CellId))

        # Add this our resultset
        poke = {
          'unique': hash,
          'id': wild.pokemon.PokemonId,
          'name': name,
          'lat': wild.Latitude,
          'lng': wild.Longitude,
          'timestamp': timestamp,
          'timeleft': wild.TimeTillHiddenMs / 1000
        }
        pokemons.append(poke)
        db.setex(key, poke['timeleft'], poke)
        db.sadd('cell:{}'.format(cell.S2CellId), key)
        # Notify
        if poke['id'] in NOTIFY_LIST_IDS:
          logger.debug('Notify discovery of {}'.format(poke))
          notify(poke, origin_lat, origin_lng)
  return pokemons


@app.task(bind=True, name='scan_cell')
def scan_cell(self, cell_id, origin_lat, origin_lng, username):
  # Scans a single levle 15 cell and its surround neighbors
  # cell_id: s2sphere.sphere.CellId
  # lat, lng: player current location
  # username: which account to use for scanning

  try:
    center = cell_id.to_lat_lng()
    set_location_coords(center.lat().degrees, center.lng().degrees, 0)
    logger.debug("Scanning Pokemons in nearby cells of {}".format(center))
    session = get_session(username)
    logger.debug('Scanning using account {}'.format(session['username']))
    httpsession = requests.session()
    httpsession.headers.update({'User-Agent': 'Niantic App'})
    httpsession.verify = False

    response = heartbeat(session, httpsession)
    logger.debug('heartbeat response: {}'.format(response))

    pokemons = []
    timestamp = int(time.time())
    for cell in response.cells:
      pokemons.extend(_process_cell_response(cell, timestamp, origin_lat, origin_lng))

    # Cache for a random amount of seconds to avoid spikes
    db.setex('cell:{}_last_scanned'.format(cell_id.id()), random.randint(*CACHE_BOUNDS), timestamp)
    db.set('cell:{}_last_scanned_by'.format(cell_id.id()), session['username'])
    # Add this cell to the geo index
    db.geoadd('locations', center.lng().degrees, center.lat().degrees, session['username'])
    session['last_scanned'] = timestamp
    update_session(session)

    logger.debug('Found {} pokemons near {}'.format(len(pokemons), center))
    return pokemons
  except PTCServerException as e:
    #  update_server_status('PTC', 'UP')
    raise e
  except POGOServerException as e:
    #  update_server_status('PTC', 'DOWN')
    raise e
  finally:
    # Remove the lock so another task can be triggered
    lock = 'cell:{}_lock'.format(cell_id.id())
    logger.debug('Removing lock {}'.format(lock))
    db.delete(lock)


def _login(httpsession, username, password):
  access_token = login_ptc(httpsession, username, password)
  logger.debug('RPC Session Token: {} ...'.format(access_token[:25]))

  try:
    api_endpoint = get_api_endpoint(httpsession, access_token)
    logger.debug('Received API endpoint: {}'.format(api_endpoint))

    profile_response = get_profile(httpsession, access_token, api_endpoint, None)
    logger.debug('Profile response: {}'.format(profile_response))

    #  if profile_response.unknown1 != 2:
      #  raise POGOServerException('Get profile bad response')
    #  payload = profile_response.payload[0]
    #  profile = pokemon_pb2.ResponseEnvelop.ProfilePayload()
    #  profile.ParseFromString(payload)
    #  logger.debug('Username: {}'.format(profile.profile.username))

    #  creation_time = datetime.fromtimestamp(int(profile.profile.creation_time) / 1000)
    #  logger.debug('You have been playing Pokemon Go since: {}'.format(creation_time.strftime('%Y-%m-%d %H:%M:%S')))
    #  for curr in profile.profile.currency:
      #  logger.debug('{}: {}'.format(curr.type, curr.amount))
    logger.debug('Login success. Username: {}, ticket expires: {}'.format(username, profile_response.unknown7.unknown72))
    return {
      'access_token': access_token,
      'api_endpoint': api_endpoint,
      'useauth': profile_response.unknown7.SerializeToString()
    }
  except Exception as e:
    logger.exception('Cannot login with PokemonGO server')
    raise POGOServerException(e)


class Connection:
  def __init__(self, username, password):
    self.username = username
    self.password = password
    self.http_session = requests.session()
    self.http_session.headers.update({'User-Agent': 'Niantic App'})
    self.http_session.verify = False

  def login(self):
    logger.debug("Logging in: {}".format(self.username))
    session = _login(self.http_session, self.username, self.password)
    session['last_login'] = time.time()
    session['username'] = self.username
    db.hmset('ptc_session_{}'.format(self.username), session)
