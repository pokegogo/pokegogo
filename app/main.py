import time
import json
from flask import request, jsonify
import geopy.distance
from s2sphere import LatLng, LatLngRect, RegionCoverer, Cell
import logging
from app import app, logger
from .exceptions import BadRequest, InvalidMove, ServerMaxCapacity
from .scanner import scan_cell, db, get_server_status, get_session


DEFAULT_RADIUS_IN_METERS = 100
MAX_TRAVEL_DISTANCE_IN_METERS = 1000
MAX_SCAN_RADIUS = 2000
ACCOUNT_LOCK_PERIOD = 5


logger.setLevel(logging.INFO)

@app.before_first_request
def setup_logging():
  if not app.debug:
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.INFO)


def get_last_scanned_time(username):
  last_scanned = db.hget('ptc_session_{}'.format(username), 'last_scanned')
  if last_scanned:
    return int(last_scanned)
  return None


def get_best_session(lat, lng):
  # Pick a login session that's best for this request to avoid teleport softban detection
  # Pick a session where the last scanned location is closest to the current cell for best locality
  # If the distance between new location and last location / period between last scan is > threshold, use it
  # 100m/second is the allowed threashold, so if distance is 1000m and it was 100 second ago, then it's safe to use
  # If distance is less than 2000 meters, always use, since this is when a single user performs the search
  # Otherwise, move onto the next account in the list
  # If no account is valid, return error (MAX_CAPACITY_REACHED)

  logger.debug('Getting best session for {}, {}'.format(lat, lng))

  # Get back the closest locations
  SEARCH_RADIUS = 20000000
  MIN_DISTANCE_THRESHOLD = 2000
  # TODO Not all of these are valid. For example an account that logged in successfully initially but failed later on
  # We need to remove the bad ones during session refreshes
  #  logger.info('Available accounts/locations: {}'.format(db.zcard('locations')))
  locations = db.georadius('locations', lng, lat, SEARCH_RADIUS, unit='m', sort='ASC', withdist=True)
  logger.debug('Locations closest to point: {}'.format(locations))
  for username, distance in locations:
    picked = False
    if distance < MIN_DISTANCE_THRESHOLD:
      picked = True
    else:
      last_scanned = get_last_scanned_time(username)
      # This account hasn't been used
      if not last_scanned:
        picked = True
      else:
        delta = time.time() - last_scanned
        logger.debug('Last scanned {} seconds ago, distance: {}, speed: {}'.format(delta, distance, distance / delta))
        if distance / delta < 100:
          picked = True
    if not picked:
      continue
    # Don't use if the account is currently locked, due to rate limit
    lock = 'account_{}_locked'.format(username)
    locked = db.setnx(lock, True)
    if locked:
      db.expire(lock, ACCOUNT_LOCK_PERIOD)
      return get_session(username)
    else:
      logger.debug('Account {} is currently locked'.format(username))
  return None


def get_cell_ids(level, lat, lng, radius):
  # Return all level 15 cells covering the region roughly with the given radius
  corner_distance = 1.5 * radius
  distance = geopy.distance.VincentyDistance(meters=corner_distance)
  center = (lat, lng, 0)
  p1 = distance.destination(point=center, bearing=45)
  p2 = distance.destination(point=center, bearing=225)
  p1 = LatLng.from_degrees(p1[0], p1[1])
  p2 = LatLng.from_degrees(p2[0], p2[1])
  rect = LatLngRect.from_point_pair(p1, p2)
  region = RegionCoverer()
  region.min_level = level
  region.max_level = level
  cells = region.get_covering(rect)
  return cells


@app.route('/load')
def load():
  lat = float(request.args.get('lat'))
  lng = float(request.args.get('lng'))

  radius = int(request.args.get('radius', DEFAULT_RADIUS_IN_METERS))
  level = int(request.args.get('level', 16))
  if radius > MAX_SCAN_RADIUS:
    raise BadRequest('Radius has to be <= {} meters'.format(MAX_SCAN_RADIUS))
  if level > 16:
    raise BadRequest('Level has to be <= 16')

  # Get all the cell IDs containing pokemons for the current location
  cell_ids = sorted(get_cell_ids(level, lat, lng, radius))
  logger.debug('Scanning {} cells, level:{}, radius:{}m, center:{},{}'.format(len(cell_ids), level, radius, lat, lng))
  level_15_cell_ids = get_cell_ids(15, lat, lng, radius)  # Pokemons are stored under level 15 cells in redis

  pokemons = []
  # Look up pokemons in our db
  for cell in level_15_cell_ids:
    pokemon_ids = list(db.smembers('cell:{}'.format(cell.id())))
    if pokemon_ids:
      pokes = db.mget(pokemon_ids)
      purge = []
      for i, poke in enumerate(pokes):
        # This pokemon expired already
        if not poke:
          purge.append(pokemon_ids[i])
        else:
          poke = eval(poke)
          # Update timeleft before returning it
          poke['timeleft'] = poke['timeleft'] - (int(time.time()) - poke['timestamp'])
          pokemons.append(poke)
      if purge:
        logger.debug('Purging {} from {}'.format(purge, cell.id()))
        db.srem('cell:{}'.format(cell.id()), *purge)

  # Queue scanning jobs for cells that we haven't scanned yet
  cells = []
  unique_accounts = set()
  total_locked = 0
  total_cached = 0
  total_scanned = 0
  total_busy = 0
  for cell in cell_ids:
    c = Cell(cell)
    path = []
    for i in range(4):
      latlng = LatLng.from_point(c.get_vertex(i))
      path.append([latlng.lat().degrees, latlng.lng().degrees])

    cell_lat = cell.to_lat_lng().lat().degrees
    cell_lng = cell.to_lat_lng().lng().degrees
    account = db.get('cell:{}_last_scanned_by'.format(cell.id()))
    cell_output = {
      'cell_id': str(cell.id()),
      'lat': cell_lat,
      'lng': cell_lng,
      'path': path,
      'last_scanned_by': account
    }
    unique_accounts.add(account)
    cell_lock_key = 'cell:{}_lock'.format(cell.id())
    if db.exists(cell_lock_key):
      cell_output['cell_status'] = 'LOCKED'
      total_locked += 1
      logger.debug('{} is locked. Unlock in {} seconds'.format(cell.id(), db.ttl(cell_lock_key)))
    elif db.exists('cell:{}_last_scanned'.format(cell.id())):
      cell_output['cell_status'] = 'CACHED'
      total_cached += 1
      logger.debug('{} has recently been updated, not going to refresh'.format(cell.id()))
    else:
      session = get_best_session(cell_lat, cell_lng)
      if not session:
        # All accounts are currently busy
        #  raise ServerMaxCapacity('The PokeGoGo server is at max capacity. Please try again later')
        cell_output['cell_status'] = 'BUSY'
        total_busy += 1
      else:
        cell_output['cell_status'] = 'SCANNED'
        total_scanned += 1
        # Lock exist for 1 minute in order for backlogs to clear
        db.setex(cell_lock_key, 60, True)
        result = scan_cell.delay(cell, lat, lng, session['username'])
        #  logger.info('Cell scanner task {} for {}'.format(result, cell.id()))
        cell_output['task_id'] = result.id
    cells.append(cell_output)

  # Server status
  response = {
    'pokemons': pokemons,
    'cells': cells,
    'server_status': get_server_status(),
    'stats': {
      'TOTAL_CELLS': len(cells),
      'TOTAL_POKEMONS': len(pokemons),
      'TOTAL_CACHED': total_cached,
      'TOTAL_SCANNED': total_scanned,
      'TOTAL_BUSY': total_busy,
      'TOTAL_LOCKED': total_locked,
      'TOTAL_ACCOUNTS': len(unique_accounts)
    }
  }
  logger.info(json.dumps(response['stats'], indent=2))
  logger.debug('Response: {}'.format(json.dumps(response, indent=2)))
  response = jsonify(**response)
  return response
