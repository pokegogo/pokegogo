import json
from flask import jsonify
from app import app, logger


class BadRequest(Exception):
  def __init__(self, *args, **kargs):
    Exception.__init__(self, *args, **kargs)
    self.data = None


class InvalidMove(BadRequest):
  def __init__(self, msg, data):
    BadRequest.__init__(self, msg)
    self.data = data


class ServerMaxCapacity(Exception):
  def __init__(self, *args, **kargs):
    Exception.__init__(self, *args, **kargs)
    self.data = None


@app.errorhandler(BadRequest)
def handle_bad_request(error):
  return _prepare_error_response(error, status_code=400)


@app.errorhandler(ServerMaxCapacity)
def handle_max_capacity(error):
  return _prepare_error_response(error, status_code=500)


def _prepare_error_response(error, status_code=500):
  output = {
    'error_code': error.__class__.__name__,
    'error_message': error.message,
    'error_data': error.data
  }
  logger.info('[{}] Response: {}'.format(status_code, json.dumps(output, indent=2)))
  response = jsonify(output)
  response.status_code = status_code
  return response
