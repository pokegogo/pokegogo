#!/bin/bash
export $(cat .env | grep -v ^# | xargs)
celery -A app.scanner worker --loglevel=INFO --concurrency=20
