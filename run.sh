#!/bin/bash
export FLASK_APP=app.main
export FLASK_DEBUG=1
export $(cat .env | grep -v ^# | xargs)
python -m flask.cli run --no-debugger
