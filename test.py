import redis

db = redis.StrictRedis(host='localhost', port=6379)
#  db.geoadd('scan_locations', -122.204195, 47.612657, 'cell_id_12345')
#  db.geoadd('scan_locations', -122.204195, 48.612657, 'cell_id_12346')
#  output = db.georadius('scan_locations', -122.204195, 47.6126, 20000000, unit='m', withdist=True, sort='ASC')
#  print db.zrank('scan_locations', 'blah')
#  print db.geopos('scan_locations', ['cell_id_blahblah'])

#  print output


keys = db.keys('ptc_session*')
for key in keys:
  db.delete(key)
